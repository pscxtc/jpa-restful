package com.example.jparestful;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class JpaRestfulDataApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpaRestfulDataApplication.class, args);
    }

}
