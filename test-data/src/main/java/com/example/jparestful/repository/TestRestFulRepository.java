package com.example.jparestful.repository;

import com.example.jparestful.api.IDataApi;
import com.example.jparestful.bo.TestRestFul;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月14日
 */
@FeignClient(name = "test-data",path = "/testRestFuls")
@RepositoryRestResource
public interface TestRestFulRepository extends IDataApi {
}
