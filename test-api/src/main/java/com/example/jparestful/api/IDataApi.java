package com.example.jparestful.api;

import com.example.jparestful.bo.TestRestFul;
import org.springframework.data.repository.PagingAndSortingRepository;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月15日
 */
public interface IDataApi extends PagingAndSortingRepository<TestRestFul,Long> {

}
