package com.example.jparestful.controller;

import cn.hutool.json.JSONUtil;
import com.example.jparestful.bo.TestRestFul;
import com.example.jparestful.utils.TestRestFulUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * TODO: 类注释
 *
 * @author chenxu
 * @since 2021年04月15日
 */
@RestController
@RequestMapping("/jpaRest")
public class JpaRestController {

    @Autowired
    private RestTemplate restTemplate;

    @Value("${restful.gateway.url}")
    private String GATEWAY_URL ;

    @RequestMapping("/testA")
    public String testA(@RequestParam("id") Long id){
        ResponseEntity<TestRestFul> testRestFulOptional =  restTemplate.getForEntity(GATEWAY_URL+ TestRestFulUtil.PATH+id,TestRestFul.class);
        return JSONUtil.toJsonStr(testRestFulOptional.getBody()) ;
    }

}
